/*jshint esversion: 6 */

;(function () {
    'use strict';

    /**
     * Load an image and get notified when its done with a callback
     * @param {String} path the path to the image
     * @param {Function} callback
     */
    function loadImage(path, callback) {
        const image = document.createElement('img');
        image.addEventListener('load', function () {
            callback(image);
        });
        image.src = path;
    }

    /**
     * A helper function for deciding whether or not two boxes collide
     * @param box the first box
     * @param other the other box
     * @returns {boolean} if collision is happening
     */
    function collides(box, other) {
        return (
                (box.x > other.x && box.x < other.x + other.width) ||
                (box.x + box.width > other.x && box.x + box.width < other.x + other.width) ||
                (other.x > box.x && other.x < box.x + box.width)
            ) &&
            (
                box.y > other.y && box.y < other.y + other.height ||
                box.y + box.height > other.y && box.y + box.height < other.y + other.height ||
                other.y > box.y && other.y < box.y + box.height
            );
    }

    /**
     * Represent the movement and facing directions of entities
     */
    const Directions = Object.freeze({
        LEFT: -1,
        RIGHT: 1
    });

    /**
     * The states that the entities may be in
     *
     * Mainly used for defining animations for the states but can be used for anything
     */
    const States = Object.freeze({
        IDLE: {
            animation: 'idle'
        },
        JUMPING: {
            animation: 'jump'
        },
        ACCELERATING: {
            animation: 'run'
        },
        RUNNING: {
            animation: 'run'
        },
        PLAYER_ATTACK: {
            animation: 'attack_swing',
            damage: 25
        },
        PLAYER_JUMP_ATTACK: {
            animation: 'attack_smash',
            damage: 1
        },
        DYING: {
            animation: 'death'
        },
        DEAD: {
            animation: 'dead'
        },
        //BAT SPECIFIC
        BAT_ATTACK: {
            animation: 'attack',
            ignoreGravity: true,
            length: 25,
            damage: 1,
            attack: {
                startX: 0,
                startY: 0,
                targetX: 0,
                targetY: 0,
                entity: {},
                time: 0,
                reset: function (startX, startY, targetX, targetY, entity) {
                    this.time = 0;
                    this.startX = startX;
                    this.startY = startY;
                    this.targetX = targetX;
                    this.targetY = targetY;
                    this.entity = entity;
                },
                progress: function () {
                    this.time += 1;
                    if (this.time > States.BAT_ATTACK.length) {
                        return true;
                    } else {

                        let entity = this.entity;
                        entity.y = this.startY + (this.targetY - this.startY) * this.time / States.BAT_ATTACK.length;
                        entity.x = this.startX + (this.targetX - this.startX) * this.time / States.BAT_ATTACK.length;

                        return false;
                    }
                }
            }
        }
    });

    class SpriteSheet {

        /**
         * A Sprite sheet that provides convenient access to rendering parts of a spritesheet
         * @param {Number} spriteWidth the width of a sprite
         * @param {Number} spriteHeight the height of a sprite
         * @param {CanvasImageSource} image the image to use for the sprites
         */
        constructor(spriteWidth, spriteHeight, image) {
            this.spriteWidth = spriteWidth;
            this.spriteHeight = spriteHeight;
            this.image = image;

            this.rowLength = Math.floor(image.width / spriteWidth);
        }

        render(spriteID, g, x, y, width, height) {

            const row = Math.floor(spriteID / this.rowLength);
            const col = spriteID % this.rowLength;

            g.drawImage(
                this.image,
                this.spriteWidth * col,
                this.spriteHeight * row,
                this.spriteWidth,
                this.spriteHeight,
                x, y, width, height
            );
        }

    }

    class AnimatedSprite {
        constructor(spriteSheet, spriteWidth, spriteHeight = spriteWidth, spriteOffsetY = 0) {
            this.spriteSheet = spriteSheet;
            this.width = spriteWidth;
            this.height = spriteHeight;
            this.offsetY = spriteOffsetY;
            this.animations = {};
        }

        animation(name, length, rStart, rEnd, lStart = rStart, lEnd = rEnd) {
            this.animations[name + Directions.RIGHT] = new Animation(
                this.spriteSheet,
                rStart,
                rEnd,
                length
            );
            this.animations[name + Directions.LEFT] = new Animation(
                this.spriteSheet,
                lStart,
                lEnd,
                length
            );
        }

        render(g, entity) {
            this.withAnimation(entity,
                animation =>
                    animation.render(g,
                        entity.x + this.width / 2, entity.y - this.height + this.offsetY,
                        this.width, this.height
                    ));
        }

        tick(entity) {
            return this.withAnimation(entity, animation => animation.tick());
        }

        withAnimation(entity, callback) {
            let animation = this.animations[entity.state.animation + entity.facing];
            if (animation !== undefined) {
                return callback(animation);
            }
            return false;
        }
    }

    class Animation {

        /**
         *
         * @param {SpriteSheet} spriteSheet the spritesheet
         * @param {Number} startSprite index of the first sprite of the animation in the spritesheet
         * @param {Number} endSprite index of the last sprite of the animation in the spritesheet
         * @param {Number} length the length of the animation in ticks
         */
        constructor(spriteSheet, startSprite, endSprite, length) {
            this.spriteSheet = spriteSheet;
            this.startSprite = startSprite;
            this.endSprite = endSprite;
            this.spriteCount = endSprite - startSprite;
            this.length = length;
            this.progress = 0;
        }

        render(g, x, y, width, height) {
            let index = this.startSprite + Math.floor(this.spriteCount * (this.progress / this.length));
            this.spriteSheet.render(index, g, x, y, width, height);
        }

        /**
         * Tick the animation forward
         *
         * @returns {Boolean} whether or not the animation rolled back to start
         */
        tick() {
            this.progress += 1;
            if (this.progress >= this.length) {
                this.progress = 0;
                return true;
            } else {
                return false;
            }
        }
    }

    class Entity {

        /**
         * Represents any entity in the game
         * This includes things like score displays etc.
         * @param x the x coordinate of the entity
         * @param y the y coordinate of the entity
         */
        constructor(x = 0, y = 0) {
            this.x = x;
            this.y = y;
        }

        /**
         * Called by the game loop to update this entity
         * @param {Game} game the game that is happening
         */
        update(game) {
            this.tick(game);
        }

        /**
         * Called by the game loop to render this entity
         * @param g the graphics context to use for rendering
         */
        render(g) {
        }

        /**
         * An updating method to be overridden by the instances
         * @param {Game} game
         */
        tick(game) {
        }

        hasHealth() {
            return this.health !== undefined;
        }

        isDead() {
            return this.health <= 0;
        }
    }

    class PhysicalEntity extends Entity {
        /**
         * Represents an entity that acts in some sort of physics obeying manner
         * @param x starting x
         * @param y starting y
         * @param frictionFactor how fast the entity slows down when idle
         * @param dragFactor how fast the entity slows down when idle in air
         * @param gravity whether or not the entity respects gravity (is not flying)
         */
        constructor(x, y, frictionFactor = 0.8, dragFactor = 1, gravity = true) {
            super(x, y);
            this.frictionFactor = frictionFactor;
            this.dragFactor = dragFactor;
            this.gravity = gravity;
            this.xSpeed = 0;
            this.ySpeed = 0;
            this.grounded = true;
        }

        /**
         * Called by the game loop to update this entity
         * @param {Game} game the game that this entity is a part of
         */
        update(game) {
            super.update(game);
            // Apply speed
            this.y += this.ySpeed;
            this.x += this.xSpeed;

            // Apply gravity
            if (this.gravity && !this.state.ignoreGravity) {
                if (!this.grounded) {
                    this.ySpeed += game.gravity;
                }
                if (this.y >= game.ground) {
                    this.y = game.ground;
                    this.ySpeed = 0;
                    this.grounded = true;
                } else {
                    this.grounded = false;
                }
            }

            // Apply deceleration
            if (this.grounded) {
                this.xSpeed *= this.frictionFactor;
            } else {
                this.xSpeed *= this.dragFactor;
            }
        }
    }


    class Bat extends PhysicalEntity {
        constructor(x, y) {
            super(x, y);
            this.state = States.IDLE;
            this.size = 40;

            this.health = 100;
            this.maxHealth = 100;

            this.speed = 1.0;

            this.sprite = new AnimatedSprite();

            this.facing = Directions.RIGHT;

            let self = this;

            loadImage('./Bat.png', function (image) {
                let batSheet = new SpriteSheet(16, 24, image);

                let sprite = new AnimatedSprite(batSheet, 50, 55);

                sprite.animation('idle', 50, 5, 10);
                sprite.animation('death', 50, 10, 15);
                sprite.animation('dead', 250, 14, 15);
                sprite.animation('attack', States.BAT_ATTACK.length, 0, 5);
                self.sprite = sprite;
            });
        }

        render(g) {
            this.sprite.render(g, this);
        }

        tick(game) {
            let animationEnded = this.sprite.tick(this);
            switch (this.state) {
                case States.DEAD:
                    break;
                case States.DYING:
                    if (animationEnded) {
                        this.state = States.DEAD;
                    }
                    break;
                case States.BAT_ATTACK:
                    if (this.state.attack.progress()) {
                        game.hit(this, this.state.damage);
                        this.state = States.IDLE;
                    }
                    break;
                default:
                    if (this.health <= 0) {
                        this.state = States.DYING;
                    } else {
                        //Approach the player
                        let player = game.player;

                        if (player.health <= 0) {
                            break;
                        }

                        let entity = this;
                        let hitbox = player.hitbox();

                        //TODO the movement isn't the most natural ever
                        let distance = hitbox.x - this.x;
                        if (Math.abs(distance) < this.size / 2) {
                            if (player.grounded) {
                                this.state = States.BAT_ATTACK;
                                this.state.attack.reset(this.x, this.y, hitbox.x, this.y + this.size, entity);
                            }
                        } else if (distance > 0) {
                            this.x += this.speed;
                        } else {
                            this.x -= this.speed;
                        }
                    }
                    break;
            }
        }

        hitbox() {
            return {
                'x': this.x + this.size * 0.75,
                'y': this.y - this.size / 2 * 3,
                'width': this.size,
                'height': this.size
            };
        }

        hitzone() {
            return this.hitbox();
        }
    }

    class Player extends PhysicalEntity {
        /**
         * The player in the game
         * @param x coordinate
         * @param y coordinate
         */
        constructor(x, y) {
            super(x, y);
            this.speed = 1.5;
            this.maxSpeed = 3;
            this.jumpSpeed = 9;
            this.size = 100;
            this.state = States.IDLE;

            this.maxHealth = 100;
            this.health = this.maxHealth;

            this.sprite = new AnimatedSprite();

            this.facing = Directions.RIGHT;

            let self = this;

            loadImage('./Minotaur.png', function (image) {

                let minotaurSheet = new SpriteSheet(
                    96,
                    96,
                    image
                );

                let sprite = new AnimatedSprite(
                    minotaurSheet,
                    self.size,
                    self.size,
                    35
                );

                //Define the animations
                sprite.animation('idle', 100, 0, 5, 100, 105);
                sprite.animation('run', 50, 10, 18, 110, 118);
                sprite.animation('attack_swing', 65, 60, 69, 160, 169);
                sprite.animation('attack_smash', 75, 30, 39, 130, 139);
                sprite.animation('jump', 200, 50, 56, 150, 156);
                sprite.animation('death', 250, 90, 96, 190, 196);
                sprite.animation('dead', 250, 95, 96, 195, 196);
                self.sprite = sprite;
            });
        }

        render(g) {
            this.sprite.render(g, this);
        }

        tick(game) {
            let animDone = this.sprite.tick(this);
            //Use animation ending as signal to switch back to other states
            if (animDone) {
                switch (this.state) {
                    case States.PLAYER_ATTACK:
                        this.state = States.IDLE;
                        break;
                    case States.PLAYER_JUMP_ATTACK:
                        this.state = States.JUMPING;
                        break;
                    case States.DYING:
                        this.state = States.DEAD;
                        break;
                }
            }

            switch (this.state) {
                case States.DEAD:
                case States.DYING:
                    break;
                case States.JUMPING:
                    if (game.key(' ') && game.ground - this.y > game.ground / 4) {
                        this.state = States.PLAYER_JUMP_ATTACK;
                        this.ySpeed = game.gravity * 5;
                    }
                    break;
                case States.PLAYER_ATTACK:
                    if (this.hitSomething) {
                        break;
                    } else {
                        this.hitSomething = true;
                    }
                    game.hit(this, this.state.damage);
                    break;
                case States.PLAYER_JUMP_ATTACK:
                    game.hit(this, this.state.damage);
                    break;
                default:
                    if (this.health <= 0) {
                        this.state = States.DYING;
                        break;
                    }
                    if (!this.grounded) {
                        this.state = States.JUMPING;
                    }
                    if (game.key('a')) {
                        this.state = States.ACCELERATING;
                        this.facing = Directions.LEFT;
                    }
                    if (game.key('d')) {
                        this.state = States.ACCELERATING;
                        this.facing = Directions.RIGHT;
                    }
                    if (game.key('w')) {
                        this.state = States.JUMPING;
                        this.ySpeed = -this.jumpSpeed;
                    }
                    if (game.key(' ')) {
                        this.hitSomething = false;
                        this.state = States.PLAYER_ATTACK;
                    }
            }
            switch (this.state) {
                case States.JUMPING:
                    if (this.grounded) {
                        this.state = States.IDLE;
                    }
                    break;
                case States.ACCELERATING:
                    if (!game.downKeys.has('a') && !game.downKeys.has('d')) {
                        this.state = States.RUNNING;
                    } else {
                        let sign = this.facing;
                        this.xSpeed = sign * Math.max(sign * this.speed + sign * this.speed, this.maxSpeed);
                        break;
                    }
                    break;
                case States.RUNNING:
                    if (this.xSpeed < 0.1) {
                        this.state = States.IDLE;
                    }
            }
        }

        hitzone() {
            return {
                'x': this.x + this.size / 2 + (this.facing + 1) / 2 * this.size / 2,
                'y': this.y - this.size / 2,
                'width': this.size / 2,
                'height': this.size / 2,
            };
        }

        hitbox() {
            return {
                'x': this.x + this.size * 0.75,
                'y': this.y - this.size / 2,
                'width': this.size / 2,
                'height': this.size / 2
            };
        }
    }

    class Game {

        /**
         * Game object controls the whole game
         * @param g the graphics context to draw the game with
         * @param width {Number} the width of the game canvas
         * @param height {Number} the height of the game canvas
         */
        constructor(g, width, height) {
            this.g = g;

            this.width = width;

            this.height = height;

            this.paused = true;

            /**
             *  Used by entities for gravity
             */
            this.ground = this.height - 40;

            /**
             * Keys that are considered down right now
             */
            this.downKeys = new Set();

            /**
             * The speed at which entities fall
             */
            this.gravity = 0.30;

            /**
             * List of all the entities in the game
             * @type {Array} of Entities
             */
            this.entities = [];

            /**
             * The score the player has achieved
             * @type {number}
             */
            this.score = 0;

            /**
             * How many ticks between bats spawning (100 UPS so 700 is 7 seconds)
             * @type {number}
             */
            this.batDelay = 700;

            /**
             * A ticker that keeps track of how long there is till next bat spawns
             * @type {number}
             */
            this.batTick = 0;

        }

        /**
         * Draw the game
         */
        draw() {
            // Clear the canvas
            this.g.clearRect(0, 0, this.width, this.height);
            // Draw ground
            this.g.fillStyle = '#875424';
            this.g.fillRect(0, this.ground, this.width, this.height - this.ground);

            //Handle paused
            this.g.fillStyle = '#c4c4c4';
            if (this.paused) {
                this.g.font = "30px Verdana";
                this.g.fillText('Paused', this.width / 2 - 50, 50);
                this.g.font = "20px Verdana";
                this.g.fillText('Press P to continue', this.width / 2 - 90, 80);
            }

            if (this.player.isDead() && !this.paused) {
                this.g.font = "30px Verdana";
                this.g.fillText('Game over', this.width / 2 - 80, 50);
                this.g.font = "20px Verdana";
                this.g.fillText('Press R to restart', this.width / 2 - 90, 80);
            }

            //Draw the score
            this.g.font = "15px Verdana";
            this.g.fillText('Score: ' + this.score, 15, 25);

            this.g.font = "15px Verdana";
            this.g.fillText('use WASD-keys for movement and space for hitting', 50, this.ground + 20);

            // Draw all entities
            this.entities.forEach(entity => {
                //Draw the actual entity
                entity.render(this.g);

                //Draw health bars
                if (entity.health !== undefined && entity.maxHealth !== undefined && entity.hitbox !== undefined && entity.health > 0) {
                    let hitbox = entity.hitbox();
                    this.g.fillStyle = 'red';
                    this.g.fillRect(hitbox.x, hitbox.y - 20, hitbox.width, 10);
                    this.g.fillStyle = 'green';
                    this.g.fillRect(hitbox.x, hitbox.y - 20, hitbox.width * (entity.health / entity.maxHealth), 10);
                    this.g.strokeStyle = 'black';
                    this.g.beginPath();
                    this.g.rect(hitbox.x, hitbox.y - 20, hitbox.width, 10);
                    this.g.stroke();
                    this.g.beginPath();
                    this.g.moveTo(hitbox.x + hitbox.width * (entity.health / entity.maxHealth), hitbox.y - 20);
                    this.g.lineTo(hitbox.x + hitbox.width * (entity.health / entity.maxHealth), hitbox.y - 10);
                    this.g.stroke();
                }
            });
        }

        /**
         * Update the game state
         */
        update() {
            if (!this.paused) {

                if (!this.player.isDead()) {
                    this.batTick += 1;

                    if (this.batTick > this.batDelay) {
                        this.batTick = 0;
                        this.spawnBat();
                    }
                }

                this.entities.forEach(entity => {
                    entity.update(this);
                });
            }
        }

        /**
         * Called at intervals to spawn more bats
         */
        spawnBat() {
            this.entities.push(new Bat(Math.random() > 0.5 ? 0 : this.width, 100));
        }

        /**
         * Brodcast a hit into the game world
         * @param {Entity} source the entity hitting
         * @param {Number} damage the damage to be applied if something were to get hit
         */
        hit(source, damage) {
            this.entities.forEach(entity => {
                if (entity !== source && entity.hasHealth() && entity.health > 0 && collides(source.hitzone(), entity.hitbox())) {
                    if (source === this.player) {
                        this.score += damage;
                    }
                    entity.health -= damage;
                }
            });
        }

        /**
         * Check if a key is currently being pressed
         * @param {string} keyCode the key in question
         * @returns {boolean} whether or not the key is down
         */
        key(keyCode) {
            return this.downKeys.has(keyCode);
        }

        /**
         * Deal with a key press
         * @param {string} keyCode
         */
        keydown(keyCode) {
            if (keyCode === 'p') {
                this.paused = !this.paused;
            } else if (keyCode === 'r') {
                this.reset();
            }
            this.downKeys.add(keyCode);
        }

        /**
         * Deal with a key release
         * @param {string} keyCode
         */
        keyup(keyCode) {
            this.downKeys.delete(keyCode);
        }

        reset(state = undefined) {
            //Clear entities
            this.entities = [];

            // Add the player
            this.player = new Player(0, 0);
            this.entities.push(this.player);

            //Reset the score
            this.score = 0;
            //Reset bat tick
            this.batTick = 0;

            if (state !== undefined) {
                //Load state
                let p = state.player;
                this.player.health = p.health;
                this.player.x = p.x;
                this.player.y = p.y;

                state.bats.forEach(batData => {
                    let bat = new Bat(batData.x, batData.y);
                    bat.health = batData.health;
                    this.entities.push(bat);
                });

                this.score = state.score;
                this.batTick = state.batTick;

            } else {
                //Use default state
                this.entities.push(new Bat(200, 0));
            }

        }

        getState() {
            let player = this.player;

            let bats = [];

            this.entities.forEach(entity => {
                if (entity instanceof Bat) {
                    bats.push({
                        health: entity.health,
                        x: entity.x,
                        y: entity.y,
                    });
                }
            });

            return {
                player: {
                    health: player.health,
                    x: player.x,
                    y: player.y
                },
                bats: bats,
                score: this.score,
                batTick: this.batTick
            };
        }

        /**
         * Start the game, sets intervals for rendering and updating
         */
        start() {

            this.reset();

            const self = this;

            // Set interval for drawing (50 FPS)
            setInterval(function () {
                self.draw();
            }, 20);

            // Set interval for updating (100 UPS)
            setInterval(function () {
                self.update();
            }, 10);

            // Add key down listeners
            document.addEventListener('keydown', function (event) {
                self.keydown(event.key);
            });

            // Add key up listeners
            document.addEventListener('keyup', function (event) {
                self.keyup(event.key);
            });
        }

    }

    const canvas = document.getElementById('gameCanvas');

    const context = canvas.getContext('2d');

    const game = new Game(context, canvas.width, canvas.height);

    window.game = game;

    game.start();

}());
